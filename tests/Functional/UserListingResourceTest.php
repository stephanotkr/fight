<?php


namespace App\Tests\Functional;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

class UserListingResourceTest extends ApiTestCase
{
    public function testCreateUserListing()
    {
        $client = self::createClient();
        $client->request('GET', 'http://127.0.0.1:8000/api/users');
        $this->assertResponseStatusCodeSame(401);
    }
}