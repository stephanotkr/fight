<?php


namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

final class CreateProfilePictureAction
{
    public function __invoke(Request $request): User
    {
        $user = $request->attributes->get('data');

        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $user->setFile($request->files->get('file'));
        $user->setUpdatedAt(new \DateTimeImmutable());
        return $user;
    }
}