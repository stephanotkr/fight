<?php


namespace App\Controller;


use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SymfonyCasts\Bundle\VerifyEmail\Exception\ExpiredSignatureException;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

const ERROR_STATUS = ['status' => 'Error'];
const VERIFIED_STATUS = ['status' => 'Verified'];
const EXPIRED_STATUS = ['status' => 'Link has expired'];

class VerifyEmailAction extends AbstractController
{

    private VerifyEmailHelperInterface $verifyEmailHelper;

    private UserRepository $userRepository;

    private EntityManagerInterface $entityManager;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(VerifyEmailHelperInterface $verifyEmailHelper, UserRepository $userRepository, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {

        $this->verifyEmailHelper = $verifyEmailHelper;
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    public function __invoke(Request $request): Response
    {
        $user = $this->userRepository->find($request->query->get('id'));
        if (!$user) {
            throw $this->createNotFoundException();
        }
        try {
            $this->verifyEmailHelper->validateEmailConfirmation(
                $request->getUri(),
                $user->getId(),
                $user->getEmail(),
            );
        } catch (VerifyEmailExceptionInterface $e) {
            if ($e instanceof ExpiredSignatureException)
            {
                return new JsonResponse(EXPIRED_STATUS, 400);
            }
            else {
                $this->logger->error($e->getMessage());
                return new JsonResponse(ERROR_STATUS, 400);
            }
        }
        $user->setIsVerified(true);
        $this->entityManager->flush();
        return new JsonResponse(VERIFIED_STATUS, 200);
    }

}
