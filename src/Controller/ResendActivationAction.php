<?php


namespace App\Controller;


use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\MailerService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

const SEND_SUCCESS_STATUS = ['status' => 'success'];
const SEND_ERROR_STATUS = ['status' => 'error'];

class ResendActivationAction extends AbstractController
{
    /**
     * @var MailerService
     */
    private MailerService $mailerService;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $tokenStorage;
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * ResendActivationAction constructor.
     * @param MailerService $mailerService
     * @param LoggerInterface $logger
     * @param TokenStorageInterface $tokenStorage
     * @param UserRepository $userRepository
     */
    public function __construct(MailerService $mailerService, LoggerInterface $logger, TokenStorageInterface $tokenStorage, UserRepository $userRepository)
    {
        $this->mailerService = $mailerService;
        $this->logger = $logger;
        $this->tokenStorage = $tokenStorage;
        $this->userRepository = $userRepository;
    }

    public function __invoke()
    {
        $user = $this->userRepository->findOneBy(['email' => $this->tokenStorage->getToken()->getUser()->getUserIdentifier()]);
        if ($user) {
            try {
                $this->mailerService->sendActivationEmail($user);
                return new JsonResponse(SUCCESS_STATUS, 200);
            } catch (\Exception $e){
                $this->logger->error($e);
            }
        }
    }

}