<?php


namespace App\Controller;


use App\Entity\Fighter;
use App\Entity\Ring;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class SpellOnAction extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function __invoke(Request $request): Fighter
    {
        /** @var Ring $data */
        $data = $request->attributes->get('data');
        $gaulois = $data->getGaulois();
        $romain = $data->getRomain();
        $gaulois->putASpellOn($romain);
        $this->manager->persist($gaulois);
        $this->manager->persist($romain);
        $this->manager->flush();
        return $gaulois;
    }
}