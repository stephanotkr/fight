<?php
namespace App\Controller;

use App\Entity\UserFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CreateDocumentAction extends AbstractController
{
    public function __invoke(Request $request): UserFile
    {
        $file = new UserFile();
        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }
        $file->setFile($request->files->get('file'));
        $file->setUpdatedAt(new \DateTimeImmutable());
        $file->setName($uploadedFile->getClientOriginalName());
        $file->setSize($uploadedFile->getSize());
        return $file;
    }
}