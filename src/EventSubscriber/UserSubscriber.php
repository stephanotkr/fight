<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use App\Service\MailerService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserSubscriber implements EventSubscriberInterface
{
    /**
     * @var PasswordHasherInterface
     */
    private $passwordHasher;
    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $tokenStorage;
    private MailerService $mailerService;

    public function __construct(UserPasswordHasherInterface $passwordHasher, TokenStorageInterface $tokenStorage, MailerService $mailerService)
    {
        $this->passwordHasher = $passwordHasher;
        $this->tokenStorage = $tokenStorage;
        $this->mailerService = $mailerService;
    }

    public function onKernelView(ViewEvent $event)
    {
        $user = $event->getControllerResult();
        if($event->getRequest()->attributes->get('_controller') == 'api_platform.action.post_collection' && $event->getRequest()->getMethod()=="POST") {
            if ($user instanceof User && $user->getPassword()) {
                $password = $this->passwordHasher->hashPassword($user, $user->getPassword());
                $user->setPassword($password);
            }
        }
    }

    public function sendCredentials(ViewEvent $event)
    {
        $user = $event->getControllerResult();
        if ($user instanceof User) {
            try {
                $this->mailerService->sendCredentialsEmail($user, 'Votre mot dans l\'application ozamba est : ');
            } catch (TransportExceptionInterface $e) {
            }
        }
    }

    public function resolveMe(RequestEvent $event)
    {
        $request = $event->getRequest();

        if ('api_users_get_item' !== $request->attributes->get('_route')) {
            return;
        }

        if ('me' !== $request->attributes->get('id')) {
            return;
        }

        if(!$this->tokenStorage->getToken()){
            return;
        }
        $user = $this->tokenStorage->getToken()->getUser();

        if (!$user instanceof User) {
            return;
        }

        $request->attributes->set('id', $user->getId());
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => [['sendCredentials', EventPriorities::PRE_VALIDATE],['onKernelView', EventPriorities::POST_VALIDATE]],
            KernelEvents::REQUEST => ['resolveMe', EventPriorities::PRE_READ],
        ];
    }
}
