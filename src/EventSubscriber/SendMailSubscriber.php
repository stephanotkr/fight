<?php

namespace App\EventSubscriber;

use App\Entity\User;
use App\Service\MailerService;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class SendMailSubscriber implements EventSubscriberInterface
{
    /**
     * @var MailerService
     */
    private MailerService $mailerService;

    public function __construct(MailerService $mailerService)
    {
        $this->mailerService = $mailerService;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof User) {
            $this->mailerService->sendActivationEmail($entity);
        }
    }
}