<?php


namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model\Operation;
use ApiPlatform\Core\OpenApi\Model\PathItem;
use ApiPlatform\Core\OpenApi\Model\RequestBody;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;

class JwtDecorator implements OpenApiFactoryInterface
{

    /**
     * @var OpenApiFactoryInterface
     */
    private OpenApiFactoryInterface $decorated;

    public function __construct(OpenApiFactoryInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['Token'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'token' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
            ],
        ]);

        $schemas['Forgot'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'email' => [
                    'type' => 'string',
                    'example' => 'johndoe@example.com',
                ],
            ],
        ]);

        $schemas['Credentials'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'username' => [
                    'type' => 'string',
                    'example' => 'johndoe@example.com',
                ],
                'password' => [
                    'type' => 'string',
                    'example' => 'password',
                ],
            ],
        ]);

        $pathItem = new PathItem(
            'JWT Token',
            null,
            null,
            null,
            null,
            new Operation(
                'postCredentialsItem',
                ['Token'],
                [
                    '200' => [
                        'description' => 'Get JWT token',
                        'content' => [
                            'application/ld+json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Token',
                                ],
                            ],
                        ],
                    ],
                ],
                'Get JWT token to login.',
                'Authentication',
                null,
                [],
                new RequestBody(
                    'Generate new JWT Token',
                    new \ArrayObject([
                        'application/ld+json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials',
                            ],
                        ],
                    ]),
            ),

        ),


        );

        $pathItemCurrentUser = new PathItem(
            'Me',
            null,
            null,
            new Operation(
                'getMeItem',
                ['User'],
                [
                    '200' => [
                        'description' => 'Get my information',
                        'content' => [
                            'application/ld+json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/User',
                                ],
                            ],
                        ],
                    ],
                ],
                'Get current user information.',
                'Resolve me',
                null,
                [],
                null,

            ),
            null,
            null,


        );

        $pathItemForgotPassword = new PathItem(
            'Forgot Password',
            null,
            null,
            null,
            null,
            new Operation(
                'postForgotPasswordItem',
                ['Forgot Password'],
                [
                    '204' => [],
                ],
                'Reset Password',
                'Authentication',
                null,
                [],
                new RequestBody(
                    'Generate new JWT Token',
                    new \ArrayObject([
                        'application/ld+json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Forgot',
                            ],
                        ],
                    ]),
                ),

            ),


        );

        $openApi->getPaths()->addPath('/api/login', $pathItem);
        $openApi->getPaths()->addPath('/api/users/me', $pathItemCurrentUser);
        $openApi->getPaths()->addPath('/api/forgot_password/', $pathItemForgotPassword);

        return $openApi;
    }


}