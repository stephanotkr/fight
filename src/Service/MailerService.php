<?php


namespace App\Service;


use App\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class MailerService
{
    /** @var MailerInterface */
    private MailerInterface $mailer;
    /**
     * @var ParameterBagInterface
     */
    private ParameterBagInterface $parameterBag;
    /**
     * @var VerifyEmailHelperInterface
     */
    private VerifyEmailHelperInterface $verifyEmailHelper;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param MailerInterface $mailer
     * @param ParameterBagInterface $parameterBag
     * @param VerifyEmailHelperInterface $verifyEmailHelper
     * @param LoggerInterface $logger
     */
    public function __construct(MailerInterface $mailer, ParameterBagInterface $parameterBag, VerifyEmailHelperInterface $verifyEmailHelper, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
        $this->verifyEmailHelper = $verifyEmailHelper;
        $this->logger = $logger;
    }

    /**
     * @param User $user
     * @throws TransportExceptionInterface
     */
    public function sendCredentialsEmail(User $user, string $message): void
    {
        $email = (new Email())
            ->subject('FIGHT PLATFORM')
            ->to($user->getEmail())
            ->from($this->parameterBag->get('email_admin'))
            ->text($message .$user->getPassword());
        $this->mailer->send($email);
    }

    /**
     * @param User $user
     */
    public function sendActivationEmail(User $user): void
    {
        $signatureComponents = $this->verifyEmailHelper->generateSignature(
            'app_verify_email',
            $user->getId(),
            $user->getEmail(),
            ['id' => $user->getId()]
        );
        $uri = $signatureComponents->getSignedUrl();
        $email = (new Email())
            ->subject('Account activation')
            ->to($user->getEmail())
            ->from($this->parameterBag->get('email_admin'))
            ->text('Verifier votre compte sur ce lien: ' . $this->parameterBag->get('url_front_verification').substr($uri, strpos($uri, '?'), strlen($uri)-1));
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->error($e);
        }
    }
}
