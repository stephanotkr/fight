<?php


namespace App\Entity;


class Round
{
    private int $id;
    private int $attaquant;
    private int $attaquee;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getAttaquant(): int
    {
        return $this->attaquant;
    }

    /**
     * @param int $attaquant
     */
    public function setAttaquant(int $attaquant): void
    {
        $this->attaquant = $attaquant;
    }

    /**
     * @return int
     */
    public function getAttaquee(): int
    {
        return $this->attaquee;
    }

    /**
     * @param int $attaquee
     */
    public function setAttaquee(int $attaquee): void
    {
        $this->attaquee = $attaquee;
    }



}