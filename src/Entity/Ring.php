<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RingRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\FightAction;
use App\Controller\SpellOnAction;

/**
 * @ApiResource(
 *     itemOperations={
 *      "get",
 *      "put",
 *      "delete",
 *      "fight_hit"={
 *              "method"="GET",
 *              "controller"=FightAction::class,
 *              "path"="/rings/{id}/hit",
 *     },
 *      "fight_put_a_spell_on"={
 *              "method"="GET",
 *              "controller"=SpellOnAction::class,
 *              "path"="/rings/{id}/speel_on",
 *     },
 *     },
 *     collectionOperations={
 *      "get",
 *      "post",
 *     }
 * )
 * @ORM\Entity(repositoryClass=RingRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Ring
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Fighter::class, inversedBy="rings")
     */
    private $gaulois;

    /**
     * @ORM\ManyToOne(targetEntity=Fighter::class, inversedBy="rings")
     */
    private $romain;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGaulois(): ?Fighter
    {
        return $this->gaulois;
    }

    public function setGaulois(?Fighter $gaulois): self
    {
        $this->gaulois = $gaulois;

        return $this;
    }

    public function getRomain(): ?Fighter
    {
        return $this->romain;
    }

    public function setRomain(?Fighter $romain): self
    {
        $this->romain = $romain;

        return $this;
    }

    public function getIsFull(): bool
    {
        if ($this->gaulois && $this->romain)
        {
            return true;
        } else return false;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAtAuto()
    {
        $this->createdAt = new \DateTimeImmutable('now');
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
