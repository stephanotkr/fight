<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FighterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=FighterRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Fighter
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $hp;

    /**
     * @ORM\Column(type="integer")
     */
    private $mp;

    /**
     * @ORM\Column(type="integer")
     */
    private $st;

    /**
     * @ORM\OneToMany(targetEntity=Ring::class, mappedBy="gaulois")
     */
    private $rings;

    public function __construct()
    {
        $this->rings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getHp(): ?int
    {
        return $this->hp;
    }

    public function setHp(int $hp): self
    {
        $this->hp = $hp;

        return $this;
    }

    public function getMp(): ?int
    {
        return $this->mp;
    }

    public function setMp(int $mp): self
    {
        $this->mp = $mp;

        return $this;
    }

    public function getSt(): ?int
    {
        return $this->st;
    }

    public function setSt(int $st): self
    {
        $this->st = $st;

        return $this;
    }

    public function hit(Fighter $fighter): self
    {
        $fighter->setHp($fighter->getHp() - $this->getSt());
        return $this;
    }

    public function putASpellOn(Fighter $fighter): self
    {
        $fighter->setHp($fighter->getHp() - $this->getMp());
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setInitialeValue()
    {
        $this->hp = 100;
        $this->mp = 30;
        $this->st = 40;
    }

    /**
     * @return Collection|Ring[]
     */
    public function getRings(): Collection
    {
        return $this->rings;
    }

    public function addRing(Ring $ring): self
    {
        if (!$this->rings->contains($ring)) {
            $this->rings[] = $ring;
            $ring->setGaulois($this);
        }

        return $this;
    }

    public function removeRing(Ring $ring): self
    {
        if ($this->rings->removeElement($ring)) {
            // set the owning side to null (unless already changed)
            if ($ring->getGaulois() === $this) {
                $ring->setGaulois(null);
            }
        }

        return $this;
    }
}
